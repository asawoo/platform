#!/bin/bash
#-------------------------------------------------------------------------------
# Copyright (c) 2014-2017 IRISA, Université de Bretagne Sud, France.
#
# This file is part of the ASAWoO project.
#
# ASAWoO is free software; you can redistribute it and/or modify it under the
# terms of the CeCILL License. See CeCILL License for more details.
#
# ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY.
#
# Author: Nicolas Le Sommer
# Version: 1.0
#-------------------------------------------------------------------------------


# This script is used to set the profile of the ASAWoO WoT runtime

#-------------------------------------------------------------------------------
# usage
#-------------------------------------------------------------------------------
usage(){
    cat << EOF
Usage:
   ./bin/dm host deploy.conf [--noshell] [--docker]

   - deploy.conf: the configuration file for the avatar deployment
     (see wot-runtime-profiles/box.conf)
EOF
}

#-------------------------------------------------------------------------------
# Read the configration file
#-------------------------------------------------------------------------------
read_config_file(){
    if [[ -r $1 ]] ; then
	. $1
    else
	echo "Error: the configuration file does not exist !!!"
	echo ""
    fi
}

#-------------------------------------------------------------------------------
# Init parameters
#-------------------------------------------------------------------------------
init_parameters(){
    hostname=$(hostname)
    date=$(date +%d-%m-%Y)
    year=$(date +%Y)
    mounth=$(date +%b)
    time=$(date +"%Y-%m-%d %H:%M:%S");
}

#-------------------------------------------------------------------------------
# create a Felix instance with a specific set of bundles
#-------------------------------------------------------------------------------
create_runtime_environment(){
    local asawoo_wot_runtime_dir=$1
    local asawoo_depencencies=$2
    local bundles=$3
    local c3po_root_dir=$4   
    local c3po_config_dir=$5
    local noshell=$6

    cp -r felix/* $asawoo_wot_runtime_dir/
    # add missing directories
    mkdir $asawoo_wot_runtime_dir/bundle
    mkdir $asawoo_wot_runtime_dir/load
    mkdir $asawoo_wot_runtime_dir/settings
    # Copy base bundles
    # but omit gogo shell bundles if noshell option
    echo "--> Creation of the ASAWoO WoT runtime."
    
    cp $asawoo_depencencies/lib/felix/*.jar $asawoo_wot_runtime_dir/bundle/
    if [ $noshell == true ]; then
        \rm $asawoo_wot_runtime_dir/bundle/org.apache.felix.gogo.command-*.jar
        \rm $asawoo_wot_runtime_dir/bundle/org.apache.felix.gogo.jline-*.jar
        \rm $asawoo_wot_runtime_dir/bundle/org.apache.felix.ipojo.gogo-*.jar
        \rm $asawoo_wot_runtime_dir/bundle/jansi-*.jar
        \rm $asawoo_wot_runtime_dir/bundle/jline-*.jar
    else
        echo "--> Installation of the GoGo Shell."
        cp $asawoo_depencencies/third-party/org.apache.felix.ipojo.gogo-1.12.0.jar $asawoo_wot_runtime_dir/bundle/
	\rm $asawoo_wot_runtime_dir/bundle/org.apache.felix.shell.tui-*.jar
    fi
    echo "--> Copy of the additionnal bundles."
    cp $asawoo_depencencies/lib/jackson/* $asawoo_wot_runtime_dir/bundle/
    cp $asawoo_depencencies/lib/vertx/* $asawoo_wot_runtime_dir/bundle/
    cp $asawoo_depencencies/third-party/vertx/asawoo.vertx-osgi* $asawoo_wot_runtime_dir/bundle/
    cp $asawoo_depencencies/lib/util/guava* $asawoo_depencencies/lib/util/javax.ws.rs* $asawoo_depencencies/lib/util/json* $asawoo_depencencies/lib/util/gson* $asawoo_wot_runtime_dir/bundle/
    cp $asawoo_depencencies/third-party/jena/* $asawoo_wot_runtime_dir/bundle/

    echo "--> Copy of the configuration file of the DTN communication support"

    # create C3PO environment
    if [ ! -d $c3po_root_dir ]; then
        mkdir -p $c3po_root_dir/conf $c3po_root_dir/logs $c3po_root_dir/cache $c3po_root_dir/data
        if [ ! -f "$c3po_config_dir/c3pocfg.json" ]; then
	    echo "The configuration file $c3po_config_dir/c3pocfg.json does not exist"
	    exit 1
        fi
        cp $c3po_config_dir/c3pocfg.json $c3po_root_dir/conf/
    fi


    # Set the profil of the ASAWoO runtime environment
    echo "--> Configuration of the runtime environment."
    local config_file=$asawoo_wot_runtime_dir/conf/config.properties
    local tmp="tempfile"
    local expr1="felix.auto.start.1="
    local expr2=$bundles
    local command="sed -e 's|#${expr1}|${expr1}${expr2}|g' $config_file"
    eval $command > $tmp
    mv $tmp $config_file

    expr1="felix.auto.install.1="
    expr2=$bundles
    command="sed -e 's|#${expr1}|${expr1}${expr2}|g' $config_file"
    eval $command | envsubst > $tmp
    mv $tmp $config_file

    # change the OBR URL
    expr1="obr.repository.url="
    expr2="obr.repository.url=$ASAWOO_CODE_REPO/index.xml"
    command="sed -e 's|^${expr1}|${expr2}|g' $config_file"
    eval $command | envsubst > $tmp
    mv $tmp $config_file
    
}

#-------------------------------------------------------------------------------
# main
#-------------------------------------------------------------------------------
if [[ $# < 1 ]]; then
    usage
else
    echo "-------------------------------------------------------------------------------"
    echo " Creation, configuration and deployment of the ASAWoO WoT Runtime"
    echo "-------------------------------------------------------------------------------"
    
    __DIR__=$(dirname "${BASH_SOURCE[0]}")

    # Sets the directories 
    . $__DIR__/env_variables.sh

    read_config_file $1
    
    # check if "noshell" option has been passed
    no_shell=false
    if [ -n "$2" -a "$2" == "--noshell" ]; then
	no_shell=true
    fi
    
    if [[ ("$asawoo_wot_runtime_addr" != "localhost" && "$asawoo_wot_runtime_addr" != "127.0.0.1" ) ]]; then
        ASAWOO_WOT_RUNTIME_DIR="/tmp/asawoo-runtime"
    fi

    if [[ -d $ASAWOO_WOT_RUNTIME_DIR ]]; then
        rm -rf $ASAWOO_WOT_RUNTIME_DIR
    fi
    mkdir $ASAWOO_WOT_RUNTIME_DIR

    create_runtime_environment $ASAWOO_WOT_RUNTIME_DIR $ASAWOO_PLATFORM_DIR/dependencies "$bundles_to_deploy" $C3PO_ROOT_DIR $C3PO_CONFIG_DIR $no_shell

    if [[ ("$asawoo_wot_runtime_addr" == "localhost" || "$asawoo_wot_runtime_addr" == "127.0.0.1" ) ]]; then
        echo "--> The ASAWoO WoT runtime has been installed on the local host in directory $ASAWOO_WOT_RUNTIME_DIR."
    else
        scp -r $ASAWOO_WOT_RUNTIME_DIR "$asawoo_wot_runtime_addr:" > /dev/null
        # TODO copy and echo of the command that must be run on the remote host
        #ssh $host -e "cd $appliance_name; java -jar bin/felix.jar &"
        echo "--> The ASAWoO WoT runtime has been installed on the remote host $asawoo_wot_runtime_addr in directory asawoo-runtime." 
    fi
    
fi
