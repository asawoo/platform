#!/bin/bash
#-------------------------------------------------------------------------------
# Copyright (c) 2014-2017 IRISA, Université de Bretagne Sud, France.
#
# This file is part of the ASAWoO project.
#
# ASAWoO is free software; you can redistribute it and/or modify it under the
# terms of the CeCILL License. See CeCILL License for more details.
#
# ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY.
#
# Author: Nicolas Bonnel, Alban Mouton
# Version: 1.0
#-------------------------------------------------------------------------------

echo "--> Install and start a new ASAWoO platform"

mkdir ~/.asawoo
curl -s https://gitlab.com/asawoo/platform/raw/master/docker/docker-compose-gitlab-install.yml -o ~/.asawoo/docker-compose.platform.yml || exit 1
docker network create --driver=bridge --subnet=192.168.3.0/24 --gateway=192.168.3.1 --ipv6 --subnet=2001:1::/64 --gateway=2001:1::1 asawoo
docker-compose -f ~/.asawoo/docker-compose.platform.yml pull || exit 1
docker stop wot-runtime && docker rm wot-runtime
docker stop semantic-repos && docker rm semantic-repos
docker-compose -f ~/.asawoo/docker-compose.platform.yml up -d || exit 1

echo ""
echo "ASAWoO - The services are running, you can :"
echo "  - check the services status : docker-compose -f ~/.asawoo/docker-compose.platform.yml ps"
echo "  - open the web server : http://localhost:9999"
echo "  - open the semantic repository (login=admin, password=admin123asawoo) : http://localhost:3030"
echo ""
