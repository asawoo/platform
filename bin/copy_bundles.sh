#!/bin/bash
#-------------------------------------------------------------------------------
# French ANR ASAWoO Project
#
# Deployment of OSGi platforms
#
# author: Lionel Touseau
# CASA research group, IRISA Laboratory, Université de Bretagne Sud
# version : 0.1.0
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# usage
#-------------------------------------------------------------------------------
usage(){
cat << EOF
Usage:
   ./bin/copy_bundles <root_bundle_dir>

   - root_bundle_dir: Typically the directory where osgi bundles are stored
EOF
}

#-------------------------------------------------------------------------------
# main
#-------------------------------------------------------------------------------

if [[ $# < 1 ]]; then
   	usage
else
	# ASAWoO version
	asawoo_version=0.2.0-SNAPSHOT

	if [[ "$ASAWOO_ROOT_DIR" == "" ]]; then
		defaultDir="$(dirname "$(pwd)")"
		export ASAWOO_ROOT_DIR=$defaultDir
		echo "Environment variable ASAWOO_ROOT_DIR is not set. Using $defaultDir as default."
	fi
	if [[ "$ASAWOO_BUNDLES" == "" ]]; then
		export ASAWOO_BUNDLES=$ASAWOO_ROOT_DIR/platform/dependencies
	fi

	# Create directory structure
	if [ ! -d $1/lib ]; then
		echo "Creating directory structure"
		# add missing directories
		mkdir -p $1/lib/vertx
		mkdir -p $1/avatar
		mkdir -p $1/app/domains
		mkdir -p $1/app/raspi
		mkdir -p $1/app/demo
	fi

	echo "Updating librairies"
	cp -r $ASAWOO_BUNDLES/lib/* $1/lib/
	cp $ASAWOO_BUNDLES/third-party/fr.c3po.framework.osgi.services-1.0.0.jar $1/lib/
	cp $ASAWOO_BUNDLES/third-party/fr.c3po.framework.osgi.implementation-1.0.0.jar $1/lib/
	cp $ASAWOO_BUNDLES/third-party/org.apache.felix.ipojo.gogo-1.12.0.jar $1/lib/felix/
#	cp osgi-bundles/ibrdtn-api-osgi-1.0.5-SNAPSHOT.jar $1/ibrdtn-api-osgi-1.0.5-SNAPSHOT.jar
	echo "Updating Vertx"
	cp $ASAWOO_BUNDLES/third-party/vertx/asawoo.vertx-osgi-3.4.0.jar $1/lib/vertx/

	echo "Updating ASAWoO core"
	cp $ASAWOO_ROOT_DIR/avatar/core/target/asawoo.avatar.core-$asawoo_version.jar $1/avatar/
	echo "Updating ASAWoO managers"
	echo "--- Capability / Functionality"
	cp $ASAWOO_ROOT_DIR/avatar/managers/capability/local/target/asawoo.manager.capability.local-$asawoo_version.jar $1/avatar/
	cp $ASAWOO_ROOT_DIR/avatar/managers/functionality/local/target/asawoo.manager.functionality.local-$asawoo_version.jar $1/avatar/
	echo "--- Avatar Manager"
	cp $ASAWOO_ROOT_DIR/avatar/managers/avatar/target/asawoo.manager.avatar-$asawoo_version.jar $1/avatar/
	echo "--- HTTP client"
	cp $ASAWOO_ROOT_DIR/avatar/managers/rest/http-client/target/asawoo.manager.rest.httpclient-$asawoo_version.jar $1/avatar/
	echo "--- DTN"
	cp $ASAWOO_ROOT_DIR/avatar/managers/dtn-com/target/asawoo.manager.dtn-$asawoo_version.jar $1/avatar/
	cp $ASAWOO_ROOT_DIR/avatar/managers/c3po-adapter/target/asawoo.manager.c3poadapter-$asawoo_version.jar $1/avatar/
	echo "--- REST registry"
	cp $ASAWOO_ROOT_DIR/avatar/managers/rest/registrar/target/asawoo.manager.rest.registrar-$asawoo_version.jar $1/avatar/
	cp $ASAWOO_ROOT_DIR/avatar/managers/rest/registry/target/asawoo.manager.rest.registry-$asawoo_version.jar $1/avatar/
#	echo "Updating Dummies"
#	cp bundles/dummy/led/target/asawoo.bundles.dummy.led.jar $1/app/
#	cp bundles/dummy/light/target/asawoo.bundles.dummy.light.jar $1/app/
	echo "Updating ASAWoO Domains"
	cp $ASAWOO_ROOT_DIR/applications/domains/led/target/asawoo.applications.domains.led-$asawoo_version.jar $1/app/domains/
	cp $ASAWOO_ROOT_DIR/applications/domains/light/target/asawoo.applications.domains.light-$asawoo_version.jar $1/app/domains/
	cp $ASAWOO_ROOT_DIR/applications/domains/moving/target/asawoo.applications.domains.moving-$asawoo_version.jar $1/app/domains/
	cp $ASAWOO_ROOT_DIR/applications/domains/motion/target/asawoo.applications.domains.motion-$asawoo_version.jar $1/app/domains/
	cp $ASAWOO_ROOT_DIR/applications/domains/location/target/asawoo.applications.domains.location-$asawoo_version.jar $1/app/domains/
	echo "Updating RasPi bundles"
	cp $ASAWOO_ROOT_DIR/applications/things/raspi/pin-manager/target/asawoo.applications.things.raspi.pinmanager-$asawoo_version.jar $1/app/raspi/
	cp $ASAWOO_ROOT_DIR/applications/things/raspi/grove-led/target/asawoo.applications.things.raspi.grove.led-$asawoo_version.jar $1/app/raspi/
	cp $ASAWOO_ROOT_DIR/applications/things/raspi/grove-lightsensor/target/asawoo.applications.things.raspi.grove.light-$asawoo_version.jar $1/app/raspi/
	cp $ASAWOO_ROOT_DIR/applications/things/raspi/gopigo/gopigo-driver/target/asawoo.applications.things.raspi.gopigo.driver-$asawoo_version.jar $1/app/raspi/
	cp $ASAWOO_ROOT_DIR/applications/things/raspi/gopigo/gopigo-functionalities/target/asawoo.applications.things.raspi.gopigo.functionalities-$asawoo_version.jar $1/app/raspi/
	
	echo "Updating GPS bundles"
	cp $ASAWOO_ROOT_DIR/applications/things/gpsd/target/asawoo.applications.things.location.gpsd-$asawoo_version.jar $1/app/
	cp $ASAWOO_ROOT_DIR/applications/things/locationlogger/target/asawoo.applications.things.locationlogger-$asawoo_version.jar $1/app/

	echo "Updating demo bundles"
	echo "--- GoPiGo"
	cp $ASAWOO_ROOT_DIR/applications/demos/gopigo/target/asawoo.applications.demo.gopigo-$asawoo_version.jar $1/app/demo/
	cp $ASAWOO_ROOT_DIR/applications/wotapps/gopigo-control/target/asawoo.applications.wotapps.gopigocontrol-$asawoo_version.jar $1/app/demo/
	echo "--- Avatars"
	cp $ASAWOO_ROOT_DIR/applications/demos/tractor/target/asawoo.applications.demo.tractor-$asawoo_version.jar $1/app/demo/
	echo "--- Light notifier"
#	cp bundles/demo/lightnotifier/target/asawoo.bundles.demo.lightnotifier.jar $1/app/demo/
	echo "--- Vineyard"
	cp $ASAWOO_ROOT_DIR/applications/demos/dtn-vineyard/sensor/target/asawoo.applications.demo.vineyard.sensor-$asawoo_version.jar $1/app/demo/
	cp $ASAWOO_ROOT_DIR/applications/demos/dtn-vineyard/row/target/asawoo.applications.demo.vineyard.row-$asawoo_version.jar $1/app/demo/
	cp $ASAWOO_ROOT_DIR/applications/demos/dtn-vineyard/sprinkler/target/asawoo.applications.demo.vineyard.sprinkler-$asawoo_version.jar $1/app/demo/
	cp $ASAWOO_ROOT_DIR/applications/demos/dtn-vineyard/weeder/target/asawoo.applications.demo.vineyard.weeder-$asawoo_version.jar $1/app/demo/
	cp $ASAWOO_ROOT_DIR/applications/things/sensor-data-receiver/target/fr.asawoo.sensor.receiver-$asawoo_version.jar $1/app/demo/

fi
