#!/bin/bash
#-------------------------------------------------------------------------------
# French ANR ASAWoO Project
#
# Deployment of OSGi platforms
#
# author: Lionel Touseau
# CASA research group, IRISA Laboratory, Université de Bretagne Sud
# version : 0.1.0
#-------------------------------------------------------------------------------

c3po_version="1.0.0"
vertx_version="3.4.0"
jrosbridge_version="0.2.0"


#-------------------------------------------------------------------------------
# main
#-------------------------------------------------------------------------------
echo "Updating C3PO framework OSGi bundle in local Maven repository"
	mvn -q org.apache.maven.plugins:maven-install-plugin:2.5.1:install-file -Dpackaging=pom -DpomFile=dependencies/third-party/fr.c3po.framework.osgi-$c3po_version.pom -Dfile=dependencies/third-party/fr.c3po.framework.osgi-$c3po_version.pom || exit 1;
	mvn -q org.apache.maven.plugins:maven-install-plugin:2.5.1:install-file -Dpackaging=jar -Dfile=dependencies/third-party/fr.c3po.framework.osgi.services-$c3po_version.jar -DpomFile=dependencies/third-party/fr.c3po.framework.osgi.services-$c3po_version.pom || exit 1;
	mvn -q org.apache.maven.plugins:maven-install-plugin:2.5.1:install-file -Dpackaging=jar -Dfile=dependencies/third-party/fr.c3po.framework.osgi.implementation-$c3po_version.jar -DpomFile=dependencies/third-party/fr.c3po.framework.osgi.implementation-$c3po_version.pom || exit 1;
echo "Updating IBR-DTN OSGi bundle in local Maven repository"
	mvn -q org.apache.maven.plugins:maven-install-plugin:2.5.1:install-file -Dpackaging=jar -Dfile=dependencies/third-party/ibrdtn-api-osgi-1.0.5-SNAPSHOT.jar || exit 1;
echo "Updating Vertx bundle for OSGi"
	mvn -q org.apache.maven.plugins:maven-install-plugin:2.5.1:install-file -Dpackaging=jar -Dfile=dependencies/third-party/vertx/asawoo.vertx-osgi-$vertx_version.jar || exit 1;
echo "Updating ROS bundle for OSGi"
	mvn -q org.apache.maven.plugins:maven-install-plugin:2.5.1:install-file -Dpackaging=jar -Dfile=dependencies/third-party/jrosbridge-$jrosbridge_version.jar || exit 1;
echo "Done."
