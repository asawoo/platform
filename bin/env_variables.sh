#!/bin/bash
#-------------------------------------------------------------------------------
# Copyright (c) 2014-2017 IRISA, Université de Bretagne Sud, France.
#
# This file is part of the ASAWoO project.
#
# ASAWoO is free software; you can redistribute it and/or modify it under the
# terms of the CeCILL License. See CeCILL License for more details.
#
# ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY.
#
# Author: Nicolas Le Sommer
# Version: 1.0
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Definition of the environment variables used in the "dm" and "asawoo" script.
#-------------------------------------------------------------------------------

if [[ "$ASAWOO_ROOT_DIR" == "" ]]; then
    defaultDir="$(dirname "$(pwd)")"
    export ASAWOO_ROOT_DIR=$defaultDir
    echo "--> Environment variable ASAWOO_ROOT_DIR is not set. Using $defaultDir as default."
fi

export ASAWOO_PLATFORM_DIR=$ASAWOO_ROOT_DIR/platform
export ASAWOO_WOT_RUNTIME_DIR=$ASAWOO_ROOT_DIR/asawoo-runtime
export ASAWOO_LOG_DIR=$ASAWOO_WOT_RUNTIME_DIR/logs
export ASAWOO_WOT_RUNTIME_PROFILES_DIR=$ASAWOO_ROOT_DIR/platform/wot-runtime-profiles

# Code repository
if [[ "$ASAWOO_CODE_REPO" == "" ]]; then
    export ASAWOO_CODE_REPO="file://$HOME/.m2/repository"
fi

# C3PO: DTN communiction support
if [[ "$C3PO_ROOT_DIR" == "" ]]; then
    export C3PO_ROOT_DIR=$ASAWOO_WOT_RUNTIME_DIR/c3po
fi

# The initial configuration file of the C3PO DTN communuication support
# This file must be copied in $C3PO_ROOT_DIR/conf
if [[ "$C3PO_CONFIG_DIR" == "" ]]; then
    export C3PO_CONFIG_DIR=$ASAWOO_PLATFORM_DIR/conf
fi

echo "  - Platform directory: $ASAWOO_PLATFORM_DIR"
echo "  - WoT runtime directory: $ASAWOO_WOT_RUNTIME_DIR"
echo "  - Code repository: $ASAWOO_CODE_REPO"
echo "  - DTN communication support directory: $C3PO_ROOT_DIR"
