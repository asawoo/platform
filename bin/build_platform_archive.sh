#!/bin/bash
#-------------------------------------------------------------------------------
# French ANR ASAWoO Project
#
# This script creates a tar.xz archive that contains a ASAWoO WoT runtime
# environment and ASAWoO semantic repositories.
#
# author: Nicolas Le Sommer
# CASA research group, IRISA Laboratory, Université de Bretagne Sud
# version : 0.1.0
#-------------------------------------------------------------------------------

# Jena Fuseki
jena_fuseki="apache-jena-fuseki"
jena_fuseki_version="3.5.0"
jena_fuseki_archive="${jena_fuseki}-${jena_fuseki_version}.tar.gz"
url_jena_fuseki="http://apache.crihan.fr/dist/jena/binaries/${jena_fuseki_archive}"
# creation of the structure of the archive
archive_dir="$PWD/asawoo"
wot_runtime_dir="platform"
semantic_repos_dir="semantic-repos"

mkdir -p $archive_dir/$wot_runtime_dir $archive_dir/$semantic_repos_dir

#-------------------------------------------------------------------------------
# WoT Rutime
#-------------------------------------------------------------------------------
./bin/install_standalone_mvn_deps.sh
cp -r dependencies $archive_dir/$wot_runtime_dir/
cp -r bin $archive_dir/$wot_runtime_dir/
cp -r felix $archive_dir/$wot_runtime_dir/
cp -r wot-runtime-profiles $archive_dir/$wot_runtime_dir/
cp -r conf $archive_dir/$wot_runtime_dir/

echo "
#!/bin/bash
cd platform
if [ ! -d 'asawoo-runtime' ]; then
   ./bin/dm wot-runtime-profiles/core-dtn.conf --noshell
fi
./bin/start.sh &
cd -" > $archive_dir/start_wot_runtime.sh

chmod u+rx $archive_dir/start_wot_runtime.sh

#-------------------------------------------------------------------------------
# Semantic repositories
#-------------------------------------------------------------------------------
cd $archive_dir/
wget $url_jena_fuseki
tar zxf $jena_fuseki_archive -C $semantic_repos_dir
mv $semantic_repos_dir/${jena_fuseki}-${jena_fuseki_version}/ $semantic_repos_dir/jena-fuseki
\rm $jena_fuseki_archive
mkdir $semantic_repos_dir/jena-fuseki/ontologies
cd $semantic_repos_dir/jena-fuseki/ontologies
wget https://gitlab.com/asawoo/semantic-repos/raw/master/ontologies/adaptation-purposes.jsonld
wget https://gitlab.com/asawoo/semantic-repos/raw/master/ontologies/asawoo-ctx-vocab.jsonld
wget https://gitlab.com/asawoo/semantic-repos/raw/master/ontologies/asawoo-vocab.jsonld
wget https://gitlab.com/asawoo/semantic-repos/raw/master/ontologies/fuseki-config.ttl
wget https://gitlab.com/asawoo/semantic-repos/raw/master/ontologies/ssn.nt
wget https://gitlab.com/asawoo/semantic-repos/raw/master/ontologies/asawoo-capabilities.jsonld
wget https://gitlab.com/asawoo/semantic-repos/raw/master/ontologies/asawoo-functionalities.jsonld
wget https://gitlab.com/asawoo/semantic-repos/raw/master/ontologies/context-model.jsonld
wget https://gitlab.com/asawoo/semantic-repos/raw/master/ontologies/rules.n3


echo "
#!/bin/bash
config_file=semantic-repos/jena-fuseki/ontologies/fuseki-config.ttl
tmp=\"tempfile\"
expr1=\"file:/jena-fuseki\"
expr2=\"file:\$PWD/semantic-repos/jena-fuseki\"
command=\"sed -e 's|\${expr1}|\${expr2}|g' \$config_file\"
eval \$command > \$tmp
mv \$tmp \$config_file

cd semantic-repos/jena-fuseki
./fuseki-server --config=ontologies/fuseki-config.ttl &
cd -
" > $archive_dir/start_semantic_repos.sh

chmod u+rx $archive_dir/start_semantic_repos.sh

