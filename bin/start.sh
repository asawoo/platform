#!/bin/bash
#-------------------------------------------------------------------------------
# Copyright (c) 2014-2017 IRISA, Université de Bretagne Sud, France.
#
# This file is part of the ASAWoO project.
#
# ASAWoO is free software; you can redistribute it and/or modify it under the
# terms of the CeCILL License. See CeCILL License for more details.
#
# ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY.
#
# Author: Nicolas Le Sommer
# Version: 1.0
#-------------------------------------------------------------------------------

__DIR__=$(dirname "${BASH_SOURCE[0]}")

# Sets the directories 
. $__DIR__/env_variables.sh

#-------------------------------------------------------------------------------
# Start ASAWoO
#-------------------------------------------------------------------------------

# create logs directory if not present
if [ ! -d $ASAWOO_LOG_DIR ]; then
    mkdir -p $ASAWOO_LOG_DIR
fi

timestamp=$(date +"%Y-%m-%d-%H_%M-%S")


# Launch ASAWoO
cd $ASAWOO_WOT_RUNTIME_DIR

# If we are running in a docker container, we must set the network configuration
# properly
if [ -n "$1" -a "$1" == "--docker" ]; then
    if [ "$(id -u)" != "0" ]; then
        echo "The network configuration must be executed as root." 1>&2
        exit 1
    fi
    sysctl net.ipv4.ip_forward=1
    sysctl net.ipv6.conf.all.forwarding=1
    ip link set eth0 multicast on
    ip -6 route add ff02::236:1:2:3/128 dev eth0 table local
    ip -6 route list table local

    # Updates C3PO environment
    # conf/c3pocfg.json
    appid="demo"
    netid="asawoo"
    nodeid=$(hostname | sed -e 's/\(.\{6\}\).*/\1/')
    nodeip=$(ip -4 addr show dev eth0 | grep inet | sed -e 's/\ *inet\ \(.*\)\/.*/\1/')
    cat $C3PO_ROOT_DIR/conf/c3pocfg.json | tr '\n' '\r' | sed -e "s/\"c3po-app-name-prefix\"\([^\}]*\"value\":[ \t]*\)\"[^\"]*\"/\"c3po-app-name-prefix\"\1\"$appid\"/" -e  "s/\"netman-network-id\"\([^\}]*\"value\":[ \t]*\)\"[^\"]*\"/\"netman-network-id\"\1\"$netid\"/" -e "s/\"network-node-id\"\([^\}]*\"value\":[ \t]*\)\"[^\"]*\"/\"network-node-id\"\1\"$nodeid\"/" -e "s/\"console-ip-address\"\([^\}]*\"value\":[ \t]*\)\"[^\"]*\"/\"console-ip-address\"\1\"$nodeip\"/" | tr '\r' '\n' > /tmp/c3pocfg.json
    \mv -f /tmp/c3pocfg.json	$C3PO_ROOT_DIR/conf/c3pocfg.json

fi


java -Dvertx.disableDnsResolver=true -Djavacpp.platform=linux-x86_64 -Djava.util.logging.config.file=conf/logging.properties -jar bin/felix.jar
#> $ASAWOO_LOG_DIR/asawoo_$timestamp.log 2> $ASAWOO_LOG_DIR/asawoo_$timestamp.err

