#!/bin/bash
#-------------------------------------------------------------------------------
# Copyright (c) 2014-2017 IRISA, Université de Bretagne Sud, France.
#
# This file is part of the ASAWoO project.
#
# ASAWoO is free software; you can redistribute it and/or modify it under the
# terms of the CeCILL License. See CeCILL License for more details.
#
# ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY.
#
# Author: Nicolas Le Sommer
# Version: 1.0
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
# Usage
#-------------------------------------------------------------------------------
usage(){
    echo -e "asawoo_docker run-platform | build-runtime | rm-runtime | start-runtime runtime_name | stop-runtime runtime_name
  run-platform: creates all docker images, and start a docker container hosting a ASAWoO WoT Runtime, and a docker container hosting the semantic repositories.
  stop-platform: Stop the docker container hosting a ASAWoO WoT Runtime, and the docker container hosting the semantic repositories.
  build-runtime: creates a new docker image hosting the ASAWoO WoT Runtime
  start-runtime: starts a new ASAWoO WoT Runtime on the local machine. The name of the runtime must be specified.
  stop-runtime: stops the ASAWoO WoT Runtime whose name is specified as parameter.
  rm-runtime: removes a ASAWoO WoT Runtime. The name of the runtime must be specified.
  rm-runtime-image: removes a ASAWoO WoT Runtime docker image. The name of the runtime must be specified.
"
}
#-------------------------------------------------------------------------------
# creation of the docker image
#-------------------------------------------------------------------------------
create_image(){
    echo "--> Build the docker container image hosting the ASAWoO WoT Runtime"
    docker build -t asawoo/runtime -f docker/Dockerfile .
}
#-------------------------------------------------------------------------------
# Start a new docker image hosting the ASAWoO WoT Runtime
#-------------------------------------------------------------------------------
start_runtime(){
    local name=$1
    ASAWOO_CODE_REPO=file:///root/.m2/repository
    echo "--> Start the docker container $name"
    docker run --privileged -d --rm --name $name -v ~/.m2/repository:/root/.m2/repository -p 8001:9999 -p 9001:9000 -e "ASAWOO_CODE_REPO=$ASAWOO_CODE_REPO" --network asawoo -ti asawoo/runtime
}

#-------------------------------------------------------------------------------
# Stop a docker image hosting the ASAWoO WoT Runtime
#-------------------------------------------------------------------------------
stop_runtime(){
    local name=$1
    echo "--> Stop the docker container $name"
    docker stop $1
}

#-------------------------------------------------------------------------------
# rm a docker container hosting the ASAWoO WoT Runtime
#-------------------------------------------------------------------------------
rm_runtime(){
    local name=$1
    echo "--> Remove the docker container $name"
    docker rm $1
}

#-------------------------------------------------------------------------------
# Destroy the docker image hosting the ASAWoO WoT Runtime
#-------------------------------------------------------------------------------
rm_runtime_image(){
    echo "--> Destroy the docker container image hosting the ASAWoO WoT Runtime"
    docker image rm -f asawoo/runtime
}
#-------------------------------------------------------------------------------
# creates all docker images, and start a docker container hosting a ASAWoO WoT
# Runtime, and a docker container hosting the semantic repositories.
#-------------------------------------------------------------------------------
run_platform(){
    echo "--> Install and start a new ASAWoO platform"
    docker network create --driver=bridge --subnet=192.168.3.0/24 --gateway=192.168.3.1 --ipv6 --subnet=2001:1::/64 --gateway=2001:1::1 asawoo
    docker stop wot-runtime && docker rm wot-runtime
    docker stop semantic-repos && docker rm semantic-repos
    docker-compose -f docker/docker-compose.yml up -d || exit 1

    echo ""
    echo "ASAWoO - The services are running, you can :"
    echo "  - check the services status : docker-compose -f docker/docker-compose.platform.yml ps"
    echo "  - open the web server : http://localhost:9999"
    echo "  - open the semantic repository (login=admin, password=admin123asawoo) : http://localhost:3030"
    echo ""
}
#-------------------------------------------------------------------------------
# Stop the docker container hosting a ASAWoO WoT Runtime, and the docker
# container hosting the semantic repositories.
#-------------------------------------------------------------------------------
stop_plaform(){
    echo "--> Stop the ASAWoO platform"
    docker-compose -f docker/docker-compose.yml stop
}
#-------------------------------------------------------------------------------
# main
#-------------------------------------------------------------------------------

if [[ $# -ne 1 ]] ; then
    usage
    exit 1
fi

case "$1" in
    run-platform)
        run_platform
        ;;
    build-runtime)
        create_image
        ;;

    start-runtime)
        if [[ $# -ne 2 ]] ; then
            usage
            exit 1
        fi
        start_runtime $2
        ;;
    stop-runtime)
        if [[ $# -ne 2 ]] ; then
            usage
            exit 1
        fi
        stop_runtime $2
        ;;
    rm-runtime)
        if [[ $# -ne 2 ]] ; then
            usage
            exit 1
        fi
        stop_runtime $2
        ;;
    rm-runtime-image)
        rm-runtime-image
        ;;
    stop-platform)
        stop_plaform
        ;;
    *)
        usage
        exit 1
        ;;
esac
