/* 
 * Copyright (c) 2014 IRISA, Université de Bretagne Sud, France.
 *
 * This file is part of the ASAWoO project.
 *
 * ASAWoO is free software; you can redistribute it and/or modify it under the
 * terms of the CeCILL License. See CeCILL License for more details.
 *
 * ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY.
 */
package lejos;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;


/**
 * 
 * @author Rémi Desmargez
 * @version 0.1.0
 */
public class Activator implements BundleActivator{
	
    @Override
    public void start(BundleContext context) throws Exception {
    	System.out.println("[LEJOS] [Start]");
    }

    @Override
    public void stop(BundleContext context) throws Exception {
    	System.out.println("[LEJOS] [Stop]");
    }

}
