# FULL COMPILE/RUN

    cd ~/asawoo/developpements/middleware
    mvn clean install
    ./bin/dm conf/local_dummy_listener.conf
    ./bin/asawoo tmp-deploy-dir/appliance-dummy


# QUICK COMPILE/RUN
    mvn install && export ASAWOO_BUNDLES=`pwd`  &&./bin/dm conf/local_dummy_listener.conf && ./bin/asawoo tmp-deploy-dir/appliance-dummy

#ECLIPSE INSTALL
Download Eclipse Luna. Start it.

In Help/Install New Software:

    http://www.fuin.org/p2-repository/
Expand "Maven osgi-bundles" and select "slf4j-api"

In Help/Install New Software:

    http://download.eclipse.org/technology/m2e/releases

File/Import/Maven/Existing Maven Projects
    point to asawoo git



# REST TESTING


## INSTALL RESTY
    sudo apt-get install curl
    mkdir ~/bin
    cd ~/bin
    curl -L http://github.com/micha/resty/raw/master/resty > resty
    echo >>~/.bashrc
    echo source ~/bin/resty >>~/.bashrc
Now restart your shell

## Go to listeners webpage
    http://localhost:8080/listeners-wot-app
    
## Connect to appliance
    resty http://localhost:8080/listeners-wot-app

## Instantiate Separate thermo sensor/increaser/decreaser + regulator
    POST / "name=thermosensor&factory=asawoo.bundles.dummy.temperature.DummyTemperatureSensorFactory&temperature-filename=/tmp/temp.txt&exposed=true"
    POST / "name=thermoincreaser&factory=asawoo.bundles.dummy.temperature.DummyTemperatureIncreaserFactory&temperature-fil    ename=/tmp/temp.txt&exposed=true"
    POST / "name=thermodecreaser&factory=asawoo.bundles.dummy.temperature.DummyTemperatureDecreaserFactory&temperature-filename=/tmp/temp.txt&exposed=true"
    POST / "name=thermoregulator&factory=asawoo.bundles.dummy.temperature.DummyTemperatureRegulatorFactory&sensor=thermosensor&increaser=thermoincreaser&decreaser=thermodecreaser&exposed=true"

## Complete thermostation + regulator
    POST / "name=thermostation&factory=asawoo.bundles.dummy.temperature.DummyTemperatureStationFactory&temperature-filename=/tmp/temp.txt&exposed=true"
    POST / "name=thermoregulator&factory=asawoo.bundles.dummy.temperature.DummyTemperatureRegulatorFactory&sensor=thermostation&increaser=thermostation&decreaser=thermostation&exposed=true"


## Temperature on Raspberry Pi
    resty http://raspberrypi.local:8080/listeners-wot-app
    POST / "name=thermosensor&factory=asawoo.bundles.raspi.pinmanager.GroveTemperatureSensorFactory&pin=0"
    POST / "name=thermoincreaser&factory=asawoo.bundles.raspi.pinmanager.GroveTemperatureIncreaserFactory&pin=3"
    POST / "name=thermodecreaser&factory=asawoo.bundles.raspi.pinmanager.GroveTemperatureDecreaserFactory&pin=3"
    POST / "name=thermoregulator&factory=asawoo.bundles.dummy.temperature.DummyTemperatureRegulatorFactory&sensor=thermosensor&increaser=thermoincreaser&decreaser=thermodecreaser"

    POST / "name=thermostation&factory=asawoo.bundles.raspi.pinmanager.GroveTemperatureStationFactory&pinsensor=0&pinincreaser=3&pindecreaser=3"
    POST / "name=thermoregulator&factory=asawoo.bundles.dummy.temperature.DummyTemperatureRegulatorFactory&sensor=thermostation&increaser=thermostation&decreaser=thermostation"



