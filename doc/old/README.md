# ASAWoO Middleware - README


# INSTALL MAVEN
sudo apt-get install maven

# INSTALL ECLIPSE
Download Eclipse Luna. Start it.

In Help/Install New Software:

    http://www.fuin.org/p2-repository/
Expand "Maven osgi-bundles" and select "slf4j-api"

In Help/Install New Software:

    http://download.eclipse.org/technology/m2e/releases

File/Import/Maven/Existing Maven Projects
    point to asawoo/developpements/middleware
    
In case of a problem with Eclipse: close eclipse, delete the workspace folder, restart eclipse with new workspace, reimport projects

# INSTALL RESTY
Resty is a nice command line tool for executing HTTP REST requests

    sudo apt-get install curl
    mkdir ~/bin
    cd ~/bin
    curl -L http://github.com/micha/resty/raw/master/resty > resty
    echo >>~/.bashrc
    echo source ~/bin/resty >>~/.bashrc
    
Now restart your shell



# USAGE
    cd ~/asawoo/developpements/middleware

# FULL COMPILE
    mvn clean package
    
# QUICK COMPILE/RUN
    mvn install && export ASAWOO_BUNDLES=`pwd`  &&./bin/dm conf/local_dummy_listener.conf && ./bin/asawoo tmp-deploy-dir/appliance-dummy

# LAUNCH SIMPLE dummy actuator/sensor scenario

    ./bin/dm conf/demo_simple.conf
    ./bin/asawoo tmp-deploy-dir/appliance-demo

To check the RESTful service registry:
    http://localhost:9999/registry

To start/stop the light notification demo (i.e., periodic check of luminosity, and switching of a led if it is too dark)
    http://localhost:9999/demo/start
    http://localhost:9999/demo/stop

To start/stop the lightsensor simulator (increases/decreases light over time)
    http://localhost:9999/dummy/lightsensor/start
    http://localhost:9999/dummy/lightsensor/stop

To manually set light value
    (PUT) http://localhost:9999/dummy/lightsensor/<value>

To check the brightness level
    http://localhost:9999/lightsensor/light
To check the led
    http://localhost:9999/ledinspector/status
To switch the led
    (PUT) http://localhost:9999/led/status/on
    (PUT) http://localhost:9999/led/status/off

# LAUNCH TEMPERATURE REGULATION SCENARIO

## Spawn Asawoo
    ./bin/dm conf/local_dummy_listener.conf
    ./bin/asawoo tmp-deploy-dir/appliance-dummy

## Connect to spawner servlet with resty and instantiate Separate thermo sensor/increaser/decreaser + regulator
    resty http://localhost:8080/dev/spawn
    POST / "name=temp-livingroom&factory=asawoo.bundles.dummy.temperature.DummyTemperatureSensorFactory&temperature-filename=/tmp/temp.txt&exposed=true"
    POST / "name=increaser-livingroom&factory=asawoo.bundles.dummy.temperature.DummyTemperatureIncreaserFactory&temperature-filename=/tmp/temp.txt&exposed=true"
    POST / "name=decreaser-livingroom&factory=asawoo.bundles.dummy.temperature.DummyTemperatureDecreaserFactory&temperature-filename=/tmp/temp.txt&exposed=true"
    POST / "name=regulator-livingroom&factory=asawoo.bundles.dummy.temperature.DummyTemperatureRegulatorFactory&sensor=temp-livingroom&increaser=increaser-livingroom&decreaser=decreaser-livingroom&exposed=true"

## Go to invoke webpage
    http://localhost:8080/dev/invoke



# LAUNCH DUMMY TEMPERATURE REGULATION SCENARIO WITH WEB

## Spawn Asawoo
    ./bin/dm conf/dummy_web.conf
    ./bin/asawoo tmp-deploy-dir/appliance-dummy

## Connect to spawner servlet with resty and instantiate Separate thermo sensor/increaser/decreaser + regulator
    resty http://localhost:8080/dev/spawn
    POST / "name=temp-livingroom&factory=asawoo.bundles.dummy.temperature.DummyTemperatureSensorFactory&temperature-filename=/tmp/temp.txt&exposed=true"
    POST / "name=increaser-livingroom&factory=asawoo.bundles.dummy.temperature.DummyTemperatureIncreaserFactory&temperature-filename=/tmp/temp.txt&exposed=true"
    POST / "name=decreaser-livingroom&factory=asawoo.bundles.dummy.temperature.DummyTemperatureDecreaserFactory&temperature-filename=/tmp/temp.txt&exposed=true"
    POST / "name=regulator-livingroom&factory=asawoo.bundles.dummy.temperature.DummyTemperatureRegulatorFactory&sensor=temp-livingroom&increaser=increaser-livingroom&decreaser=decreaser-livingroom&exposed=true"

## Go to Asawoo main page 
    http://localhost:8080/asawoomainpagewotapp








# LAUNCH TEMPERATURE REGULATION SCENARIO WITH COMPLETE THERMOSTATION

## Spawn Asawoo
    ./bin/dm conf/local_dummy_listener.conf
    ./bin/asawoo tmp-deploy-dir/appliance-dummy

## Connect to spawner servlet with resty and instantiate Complete thermostation + regulator
    resty http://localhost:8080/dev/spawn
    POST / "name=thermostation&factory=asawoo.bundles.dummy.temperature.DummyTemperatureStationFactory&temperature-filename=/tmp/temp.txt&exposed=true"
    POST / "name=thermoregulator&factory=asawoo.bundles.dummy.temperature.DummyTemperatureRegulatorFactory&sensor=thermostation&increaser=thermostation&decreaser=thermostation&exposed=true"

## Go to invoke webpage
http://localhost:8080/dev/invoke



# LAUNCH TEMPERATURE REGULATION SCENARIO ON RASPI

## Spawn Asawoo
    ./bin/dm conf/local_dummy_listener.conf
    ./bin/asawoo tmp-deploy-dir/appliance-dummy

## Separate sensor/increaser/decreaser + regulator
    resty http://raspberrypi.local:8080/spawn
    POST / "name=thermosensor&factory=asawoo.bundles.raspi.pinmanager.GroveTemperatureSensorFactory&pin=0"
    POST / "name=thermoincreaser&factory=asawoo.bundles.raspi.pinmanager.GroveTemperatureIncreaserFactory&pin=3"
    POST / "name=thermodecreaser&factory=asawoo.bundles.raspi.pinmanager.GroveTemperatureDecreaserFactory&pin=3"
    POST / "name=thermoregulator&factory=asawoo.bundles.dummy.temperature.DummyTemperatureRegulatorFactory&sensor=thermosensor&increaser=thermoincreaser&decreaser=thermodecreaser"

## Full thermostation + regulator
    resty http://raspberrypi.local:8080/spawn
    POST / "name=thermostation&factory=asawoo.bundles.raspi.pinmanager.GroveTemperatureStationFactory&pinsensor=0&pinincreaser=3&pindecreaser=3"
    POST / "name=thermoregulator&factory=asawoo.bundles.dummy.temperature.DummyTemperatureRegulatorFactory&sensor=thermostation&increaser=thermostation&decreaser=thermostation"




# LAUNCH ZWAVE SCENARIO

## Launch ZWave Server
    cd ~/asawoo/developpements/zwave/ozw-network
    ./ozw-network

## Spawn Asawoo
    ./bin/dm conf/zwave.conf
    ./bin/asawoo tmp-deploy-dir/appliance-dummy

    # Just do when you are in the middleware folder: 
    mvn clean package && ./bin/dm conf/zwave.conf && ./bin/asawoo tmp-deploy-dir/appliance-dummy/


## Call pollingdatacollector :
    resty http://localhost:8080
    POST /dev/spawn "name=datacollector&factory=asawoo.bundles.pollingdatacollector.DataCollectorFactory&period=3&exposed=true"

    ### SwitchInspector getStatus
    GET /dev/invoke -q "_appliance=datacollector&_method=monitor&functionality=asawoo.domains.plug.SwitchInspector&method=getStatus"

    ### PowerMeter getPowerLevel 
    GET /dev/invoke -q "_appliance=datacollector&_method=monitor&functionality=asawoo.domains.plug.PowerMeter&method=getPowerLevel"

    Copy past this :
    POST /dev/spawn "name=datacollector&factory=asawoo.bundles.pollingdatacollector.DataCollectorFactory&period=3&exposed=true" 

    GET /dev/invoke -q "_appliance=datacollector&_method=monitor&functionality=asawoo.domains.plug.SwitchInspector&method=getStatus" && GET /dev/invoke -q "_appliance=datacollector&_method=monitor&functionality=asawoo.domains.plug.PowerMeter&method=getPowerLevel"

## Data collector visualization
    http://localhost:8080/data?_appliance=datacollector&_method=getValuesAsJSON

## Node manager
    POST /dev/spawn "name=ZwaveNodeManager&factory=asawoo.bundles.zplug.NodeManagerFactory&exposed=true"

## Go to invoke webpage
    http://localhost:8080/dev/invoke

## Go to Asawoo main page 
    http://localhost:8080/asawoomainpagewotapp


# NOTIFICATIONS

Websocket notification relay:
    websockify 0.0.0.0:11313 localhost:11312

# CONTEXT ADAPTATION DEMO

./bin/dm conf/demo_ctxtadapt.conf && ./bin/asawoo tmp-deploy-dir/appliance-dummy



