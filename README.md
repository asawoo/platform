# WoT platforms supporting the execution of avatars

Document [ASAWoO_Project.md](doc/ASAWoO_Project.md) gives an
overview of the ASAWoO project, defines the terms used in this project, presents
the architecture of an avatar and of the ASAWoO platform, and presents the use
cases considered in this project.

This document should be read before using, or developing with, the ASAWoO
solution.

--------
## Documentation

The project's documentation can be found [here](https://asawoo.gitlab.io/), including the users' guide and developers' guide.

## Examples and demonstrations

### Video Surveillance and Sensor Data Collection using a robot
In this demo, we show how to collect data from sensors deployed in a area using
a robot that patrols in this area to perform a video surveillance.
To run this demo, first you need to load the iptables for ipv6 on your host machine:

    sudo modprobe ip_tables
    sudo modprobe ip6table_filter

And then you must run the following command:

    ./demos/turtle-patrol-collect/run.sh

Visualizing the deployed avatars the admin Wotapp of a platform :
http://localhost:9000/wotapps/admin/

Manual keyboard control of the turtle can be done by running the following commands

    docker exec -it turtlepatrolcollect_ros-turtle_1 bash
    source /ws/devel/setup.bash
    rosrun turtlesim turtle_teleop_key
    # Then use arrow keys to control the turtle, !!! Make sure this console has the focus, not the turtlesim window!!!


### ROS
In this demo, we show how a ROS-based robot can be controled using the ASAWoO
platform.

To run this demo, you must start a ROS robot on your local machine (see Docker
image in devices/ros/ros-turtlesim). Then you must run the following command:

    ./bin/dm wot-runtime-profiles/demo_ros.conf && ./bin/start.sh

### Dummy GoPiGo
In this demo, we show how to control a [GoPiGo
robot](https://www.dexterindustries.com/gopigo3/) using the ASAWoO platform.

To run this demo, you must run the following command:

    ./bin/dm wot-runtime-profiles/demo_dummy_gopigo.conf && ./bin/start.sh

List the wotapps on http://localhost:9999/wotapps/

Visualization on http://localhost:9999/wotapps/gopigo/

### Reasoning and context adaptation demo
In this demo, we show how the ASAWoO platform performs dynamic context
adaptation.

To run this demo, you must run the following command:

    ./bin/dm wot-runtime-profiles/demo_ctxtadapt.conf && ./bin/start.sh
