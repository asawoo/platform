#!/bin/bash

echo ""
echo "##########################################"
echo "# Build and run turtlesim docker container"
echo "##########################################"
echo ""


echo ""
echo "##################################################"
echo "# Run mini web server to receive ping notification"
echo "##################################################"
echo ""

while echo -ne "HTTP/1.0 200 OK\r\nContent-Length:4\r\n\r\nOK\r\n" | nc -l 8585; do echo ""; done &

echo ""
echo "####################################################"
echo "# Send device configuration to local Asawoo platform"
echo "####################################################"
echo ""
    
curl -XPOST http://localhost:9999/deviceconfigurations -d '{
  "@graph":[{
      "@id":"asawoo-d:HkLCIyR3W",
      "@type":"ssn:System",
      "label":"Husky",
      "implements":["asawoo-d:HJqKty0nb","asawoo-d:rJMqYk0hb", "asawoo-d:pos", "asawoo-d:nav", "asawoo-d:ryAxPMAhZ"]
    },{
      "@id":"asawoo-d:HJqKty0nb"
      ,"@type":"asawoo-c:ROSMotion",
      "rosMasterIP":"localhost",
      "cmdTopic":"/cmd_vel"
    },{
      "@id":"asawoo-d:rJMqYk0hb",
      "@type":"asawoo-c:ROSCamera",
      "rosMasterIP":"localhost",
      "imageTopic":"/camera/rgb/image_raw"
    },{
      "@id":"asawoo-d:pos",
      "@type":"asawoo-c:ROSPositionOdom",
      "rosMasterIP":"localhost",
      "odomTopic":"/husky_velocity_controller/odom"
    },{
      "@id":"asawoo-d:nav",
      "@type":"asawoo-c:ROSNavigationOdom",
      "rosMasterIP":"localhost",
      "cmdTopic":"/cmd_vel",
      "odomTopic":"/husky_velocity_controller/odom"
    },{
      "@id":"asawoo-d:ryAxPMAhZ",
      "@type":"asawoo-c:HTTPPingNotification",
      "httpPingNotificationUrl":"http://localhost:8585"
    }],
  "@context":{
    "asawoo-d":"http://liris.cnrs.fr/asawoo/devices#",
    "asawoo-c":"http://liris.cnrs.fr/asawoo/capabilities#",
    "ssn":"http://www.w3.org/ns/ssn/",
    "implements":{"@id":"ssn:implements","@type":"@id"},
    "label":{"@id":"http://www.w3.org/2000/01/rdf-schema#label"},
    "httpPingNotificationUrl":{"@id":"asawoo-c:httpPingNotificationUrl"},
    "rosMasterIP":{"@id":"asawoo-c:rosMasterIP"},
    "imageTopic":{"@id":"asawoo-c:imageTopic"},
    "cmdTopic":{"@id":"asawoo-c:cmdTopic"},
    "poseTopic":{"@id":"asawoo-c:poseTopic"},
    "odomTopic":{"@id":"asawoo-c:odomTopic"}
  }
}'

echo ""
echo "You can check/modify device configuration here: http://localhost:9999/wotapps/admin/#/devices"
sleep 4

