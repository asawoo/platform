#!/bin/bash

echo ""
echo "##########################################"
echo "# Build and run turtlesim docker container"
echo "##########################################"
echo ""

docker build -t turtlesim ../devices/ros/ros-turtlesim
xhost + # allows docker to use local XWindow Server
docker rm -f turtlesim
docker run \
    -it \
    -d \
    --name turtlesim \
    --net host \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    --device=/dev/video0 \
    -e DISPLAY=unix$DISPLAY \
    -v /dev/shm:/dev/shm \
    turtlesim
sleep 6

echo ""
echo "##################################################"
echo "# Run mini web server to receive ping notification"
echo "##################################################"
echo ""

while echo -ne "HTTP/1.0 200 OK\r\nContent-Length:4\r\n\r\nOK\r\n" | nc -l 8585; do echo ""; done &

echo ""
echo "####################################################"
echo "# Send device configuration to local Asawoo platform"
echo "####################################################"
echo ""

curl -XPOST http://localhost:9999/deviceconfigurations -d '{
  "@graph":[{
      "@id":"asawoo-d:HkLCIyR3W",
      "@type":"ssn:System",
      "label":"Turtle Sim",
      "implements":["asawoo-d:HJqKty0nb","asawoo-d:rJMqYk0hb", "asawoo-d:nav1", "asawoo-d:ryAxPMAhZ", "asawoo-d:datacollector1", "asawoo-d:pos"]
    },{
      "@id":"asawoo-d:HJqKty0nb"
      ,"@type":"asawoo-c:ROSMotion",
      "rosMasterIP":"localhost",
      "cmdTopic":"/turtle1/cmd_vel"
    },{
      "@id":"asawoo-d:rJMqYk0hb",
      "@type":"asawoo-c:ROSCamera",
      "rosMasterIP":"localhost",
      "imageTopic":"/image_raw"
    },{
      "@id":"asawoo-d:nav1",
      "@type":"asawoo-c:ROSTurtleNavigation",
      "rosMasterIP":"localhost",
      "cmdTopic":"/turtle1/cmd_vel",
      "poseTopic":"/turtle1/pose"
    },{
      "@id":"asawoo-d:pos",
      "@type":"asawoo-c:ROSTurtlePosition",
      "rosMasterIP":"localhost",
      "poseTopic":"/turtle1/pose"
    },{
      "@id":"asawoo-d:ryAxPMAhZ",
      "@type":"asawoo-c:HTTPPingNotification",
      "httpPingNotificationUrl":"http://localhost:8585"
    },{
      "@id":"asawoo-d:datacollector1",
      "@type":"asawoo-c:HTTPDataCollector"
    }],
  "@context":{
    "asawoo-d":"http://liris.cnrs.fr/asawoo/devices#",
    "asawoo-c":"http://liris.cnrs.fr/asawoo/capabilities#",
    "ssn":"http://www.w3.org/ns/ssn/",
    "implements":{"@id":"ssn:implements","@type":"@id"},
    "label":{"@id":"http://www.w3.org/2000/01/rdf-schema#label"},
    "httpPingNotificationUrl":{"@id":"asawoo-c:httpPingNotificationUrl"},
    "rosMasterIP":{"@id":"asawoo-c:rosMasterIP"},
    "imageTopic":{"@id":"asawoo-c:imageTopic"},
    "cmdTopic":{"@id":"asawoo-c:cmdTopic"},
    "poseTopic":{"@id":"asawoo-c:poseTopic"}
  }
}'

echo ""
echo "You can check/modify device configuration here: http://localhost:9999/wotapps/admin/#/devices"
sleep 6

echo ""
echo "#################"
echo "# Install Wotapps"
echo "#################"
echo ""

curl -XPATCH http://localhost:9999/wotapps/patrolsurveillance -d '{"active": true}'
curl -XPATCH http://localhost:9999/wotapps/passivedatacollect -d '{"active": true}'

echo ""
echo "You can check Wotapps installations here: http://localhost:9999/wotapps/admin/#/wotapps"
echo "You can go to the Patrol Surveillance Wotapp here: http://localhost:9999/wotapps/patrolsurveillance/"
echo "You can go to the Passive Data Collect Wotapp here: http://localhost:9999/wotapps/passivedatacollect/"
sleep 6

echo ""
echo "#####################################"
echo "# Configure top level functionalities"
echo "#####################################"

curl -XPUT http://localhost:9999/hklciyr3w/patrolsurveillance/PatrolSurveillanceImpl-0/itinerary -d '{"itinerary": [{
  "latitude": "2",
  "longitude": "2"
}, {
  "latitude": "10",
  "longitude": "2"
}, {
  "latitude": "10",
  "longitude": "10"
}, {
  "latitude": "2",
  "longitude": "10"
}]}'

curl -XPUT http://localhost:9999/hklciyr3w/base/BaseImpl-0/location -d '{"location":{"latitude":"7","longitude":"5"}}'
