#!/bin/bash
#-------------------------------------------------------------------------------
# Copyright (c) 2014-2017 IRISA, Université de Bretagne Sud, France.
#
# This file is part of the ASAWoO project.
#
# ASAWoO is free software; you can redistribute it and/or modify it under the
# terms of the CeCILL License. See CeCILL License for more details.
#
# ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY.
#
# Author: Alban Mouton, Nicolas Le Sommer
# Version: 1.0
#-------------------------------------------------------------------------------

__DIR__=$(dirname "${BASH_SOURCE[0]}")

cd $__DIR__
ASAWOO_PLATFORM_DIR="$(dirname "$(pwd)" | sed -e 's/\(.*\)\/.*/\1/')"
ASAWOO_SEMANTIC_REPOS_DIR="$(dirname "$(pwd)" | sed -e 's/\(.*\)\/.*\/.*/\1\/semantic-repos/')"
cd - > /dev/null

#ASAWOO_CODE_REPO="http://code-repo-http"
ASAWOO_CODE_REPO="file:///root/.m2/repository"

echo "-------------------------------------------------------------------------------"
echo " Demo: Turtle Patrol and Data Collect"
echo "-------------------------------------------------------------------------------"
asawoo_net=$(docker network ls | grep "asawoo")
if [ "$asawoo_net" == "" ]; then
  echo "--> Creation of the ASAWoO Docker Network"
  # create a network to exchange data between the objects
  docker network rm asawoo
  docker network create --driver=bridge --subnet=192.168.3.0/24 --gateway=192.168.3.1 --ipv6 --subnet=2001:1::/64 --gateway=2001:1::1 asawoo
fi

# Docker image of the ASAWoO WoT runtime
asawoo_runtime_docker_image=$(docker image ls | grep "asawoo/runtime")
if [ "$asawoo_runtime_docker_image" == "" ]; then
    echo "--> Build the docker container image hosting the ASAWoO WoT Runtime"
    cd $ASAWOO_PLATFORM_DIR
    docker build -t asawoo/runtime -f docker/Dockerfile .
    cd -
fi

# Docker image of the ASAWoO semantic repositories
asawoo_semantic_repos_docker_image=$(docker image ls | grep "asawoo/semantic-repos")
if [ "$asawoo_semantic_repos_docker_image" == "" ]; then
    echo "--> Build a docker container image hosting the semantic repositories"
    cd $ASAWOO_SEMANTIC_REPOS_DIR
    docker build -t asawoo/semantic-repos .
    cd -
fi

echo -e "\n--> Run all docker containers for the demo"

xhost + # allows docker to use local XWindow Server
docker-compose -f $__DIR__/docker-compose.yml stop
docker-compose -f $__DIR__/docker-compose.yml up -d
sleep 30

echo -e "\n--> Send device configurations to all Asawoo runtimes"

curl -XPOST http://localhost:8001/deviceconfigurations -d '{
  "@graph":[{
      "@id":"asawoo-d:robot",
      "@type":"ssn:System",
      "label":"Scout Robot",
      "implements":["asawoo-d:motion1","asawoo-d:camera1", "asawoo-d:nav1", "asawoo-d:datacollector1", "asawoo-d:pos1"]
    },{
      "@id":"asawoo-d:motion1"
      ,"@type":"asawoo-c:ROSMotion",
      "rosMasterIP":"ros-turtle",
      "cmdTopic":"/turtle1/cmd_vel"
    },{
      "@id":"asawoo-d:camera1",
      "@type":"asawoo-c:ROSCamera",
      "rosMasterIP":"ros-turtle",
      "imageTopic":"/image_raw"
    },{
      "@id":"asawoo-d:nav1",
      "@type":"asawoo-c:ROSTurtleNavigation",
      "rosMasterIP":"ros-turtle",
      "cmdTopic":"/turtle1/cmd_vel",
      "poseTopic":"/turtle1/pose"
    },{
      "@id":"asawoo-d:pos1",
      "@type":"asawoo-c:ROSTurtlePosition",
      "rosMasterIP":"ros-turtle",
      "poseTopic":"/turtle1/pose"
    },{
      "@id":"asawoo-d:datacollector1",
      "@type":"asawoo-c:DummyDataCollector"
    }],
  "@context":{
    "asawoo-d":"http://liris.cnrs.fr/asawoo/devices#",
    "asawoo-c":"http://liris.cnrs.fr/asawoo/capabilities#",
    "ssn":"http://www.w3.org/ns/ssn/",
    "implements":{"@id":"ssn:implements","@type":"@id"},
    "label":{"@id":"http://www.w3.org/2000/01/rdf-schema#label"},
    "rosMasterIP":{"@id":"asawoo-c:rosMasterIP"},
    "imageTopic":{"@id":"asawoo-c:imageTopic"},
    "cmdTopic":{"@id":"asawoo-c:cmdTopic"},
    "poseTopic":{"@id":"asawoo-c:poseTopic"}
  }
}'

curl -XPOST http://localhost:8002/deviceconfigurations -d '{
  "@graph":[{
      "@id":"asawoo-d:sensor1",
      "@type":"ssn:System",
      "label":"Sensor 1",
      "implements":["asawoo-d:sensorcapab1","asawoo-d:gpscap1"]
    },
    {
      "@id":"asawoo-d:sensorcapab1",
      "@type":"asawoo-c:DummyGenericDataSensor",
      "dataType":"Temperature",
      "dataInterval":"30000",
      "dataBuffer":"100"
    },
    {
      "@id":"asawoo-d:gpscap1",
      "@type":"asawoo-c:FixedLocationProvider",
      "locationLat":"47.644795",
      "locationLong":"-2.776605"
    }],
  "@context":{
    "asawoo-d":"http://liris.cnrs.fr/asawoo/devices#",
    "asawoo-c":"http://liris.cnrs.fr/asawoo/capabilities#",
    "ssn":"http://www.w3.org/ns/ssn/",
    "implements":{"@id":"ssn:implements","@type":"@id"},
    "label":{"@id":"http://www.w3.org/2000/01/rdf-schema#label"},
    "dataType":{"@id":"asawoo-c:dataType"},
    "dataInterval":{"@id":"asawoo-c:dataInterval"},
    "dataBuffer":{"@id":"asawoo-c:dataBuffer"},
    "locationLat":{"@id":"asawoo-c:locationLat"},
    "locationLong":{"@id":"asawoo-c:locationLong"}
  }
}'

curl -XPOST http://localhost:8003/deviceconfigurations -d '{
  "@graph":[{
      "@id":"asawoo-d:sensor2",
      "@type":"ssn:System",
      "label":"Sensor 2",
      "implements":["asawoo-d:sensorcapab2"]
    },{
      "@id":"asawoo-d:sensorcapab2",
      "@type":"asawoo-c:DummyGenericDataSensor",
      "dataType":"Gas",
      "dataInterval":"30000",
      "dataBuffer":"100"
    }],
  "@context":{
    "asawoo-d":"http://liris.cnrs.fr/asawoo/devices#",
    "asawoo-c":"http://liris.cnrs.fr/asawoo/capabilities#",
    "ssn":"http://www.w3.org/ns/ssn/",
    "implements":{"@id":"ssn:implements","@type":"@id"},
    "label":{"@id":"http://www.w3.org/2000/01/rdf-schema#label"},
    "dataType":{"@id":"asawoo-c:dataType"},
    "dataInterval":{"@id":"asawoo-c:dataInterval"},
    "dataBuffer":{"@id":"asawoo-c:dataBuffer"}
  }
}'

sleep 6

echo  -e "\n--> Install Wotapps on the simulated robot"

curl -XPATCH http://localhost:8001/wotapps/patrolsurveillance -d '{"active": true}'
curl -XPATCH http://localhost:8001/wotapps/passivedatacollect -d '{"active": true}'

#echo  -e "\n--> Install Wotapps on the base"

#curl -XPATCH http://localhost:8000/wotapps/patrolsurveillance -d '{"active": true}'
#curl -XPATCH http://localhost:8000/wotapps/passivedatacollect -d '{"active": true}'

sleep 10

echo -e "\n--> Spawning environment in turtle"
echo "base 6 6 1.57 basesmall.png"  | nc localhost 5005
echo "sensor1 2 2 1.57 sensorsmall.png"  | nc localhost 5005
echo "sensor2 10 2 1.57 sensorsmall.png"  | nc localhost 5005
echo "sensor3 10 10 1.57 sensorsmall.png"  | nc localhost 5005
echo "sensor4 2 10 1.57 sensorsmall.png"  | nc localhost 5005

echo  -e "\n--> Start positions client script controlling connectivity"
python positions_client.py >positions.log 2>&1 &

echo -e "\n--> Configure top level functionalities on the simulated robot"

curl -XPUT http://localhost:8001/robot/patrolsurveillance/PatrolSurveillanceImpl-0/itinerary -d '{"itinerary": [{
  "latitude": "2",
  "longitude": "2"
}, {
  "latitude": "10",
  "longitude": "2"
}, {
  "latitude": "10",
  "longitude": "10"
}, {
  "latitude": "2",
  "longitude": "10"
}]}'

curl -XPUT http://localhost:8001/robot/base/BaseImpl-0/location -d '{"location":{"latitude":"6","longitude":"6"}}'
echo -e"\-------------------------------------------------------------------------"
echo "Check the admin Wotapp of the base here : http://localhost:8000/wotapps/admin/#/wotapps"
echo "Check the C3PO console of the base this way : telnet localhost 9000"
echo "Check the admin Wotapp of the simulated robot here : http://localhost:8001/wotapps/admin/#/wotapps"
echo "Check logs of positions_client in positions_client.log"
echo "Check logs of services : docker-compose -f $__DIR__/docker-compose.yml logs"
echo "Stop all containers : docker-compose -f $__DIR__/docker-compose.yml stop"
