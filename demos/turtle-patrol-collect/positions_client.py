#!/usr/bin/env python
import time
import sys
import math
import socket
import json
import os

from threading import Thread


TCP_IP = 'localhost'
TCP_PORT = 5005
RANGE_RADIUS = 2

with open("container_associations.json","r") as f:
    container_associations = json.load(f)

for name in container_associations:
    cid = container_associations[name]
    fname = "%s.ip"%cid
    cmd = "docker exec %s ifconfig eth0  | grep \"inet6 200\" > %s "%(cid,fname)
    os.system(cmd)
    with open(fname,"r") as f:
        line = f.read().strip()
        print "line",line
        print "toks",line.split(" ")
        ip = line.split(" ")[1]
        container_associations[name] = {"id":cid,"ip":ip}


print "container_associations"
print json.dumps(container_associations,indent=4)
        

def get_dist(pa,pb):
    dx = pa[0]-pb[0]
    dy = pa[1]-pb[1]
    return math.sqrt(dx**2 + dy**2)

class PositionsClient:

    def __init__(self,ip,port):
        self.ip = ip
        self.port = port
        print self.acquire()
        print self.compute_distances()
        while True:
            self.compute_distances()
            time.sleep(1)            



    def compute_distances(self):
        positions = self.acquire()
        print "distances"
        for a in positions.keys():
            for b in positions.keys():
                if a!=b:
                    dist = get_dist(positions[a],positions[b])
                    print a," - ",b, "dist",dist,"connected",dist < RANGE_RADIUS
                    self.set_link_active(a,b,dist < RANGE_RADIUS)


        return positions

    def set_link_active(self,a,b,active):
        if a not in container_associations:
            print "Cannot find container for device ",a
            return
        if b not in container_associations:
            print "Cannot find container for device ",b
            return

        if active:

            print "Activating link for ",a,"-",b
            #os.system("docker exec ...")
            cmd = "docker exec %s ip6tables -I INPUT -s %s -j ACCEPT"%(container_associations[a]["id"],container_associations[b]["ip"])
            os.system(cmd)
            cmd = "docker exec %s ip6tables -I INPUT -s %s -j ACCEPT"%(container_associations[b]["id"],container_associations[a]["ip"])
            os.system(cmd)

            #cmd = "docker exec %s ping6 -c1 %s"%(container_associations[a]["id"],container_associations[b]["ip"])
            #os.system(cmd)
            #cmd = "docker exec %s ping6 -c1 %s"%(container_associations[b]["id"],container_associations[a]["ip"])
            #os.system(cmd)
        else:
            print "Deactivating link for ",a,"-",b
            #os.system("docker exec ...")
            cmd = "docker exec %s ip6tables -I INPUT -s %s -j DROP"%(container_associations[a]["id"],container_associations[b]["ip"])
            os.system(cmd)
            cmd = "docker exec %s ip6tables -I INPUT -s %s -j DROP"%(container_associations[b]["id"],container_associations[a]["ip"])
            os.system(cmd)

            #cmd = "docker exec %s ping6 -c1 %s"%(container_associations[a]["id"],container_associations[b]["ip"])
            #os.system(cmd)
            #cmd = "docker exec %s ping6 -c1 %s"%(container_associations[b]["id"],container_associations[a]["ip"])
            #os.system(cmd)
        print cmd
        



    def acquire(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        self.socket.connect((self.ip,self.port))
        #self.socket.setblocking(0)
        self.socket.setsockopt( socket.IPPROTO_TCP, socket.TCP_NODELAY, 1 )
        self.socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_QUICKACK, 1)
        time.sleep(0.5)
        data = self.socket.recv(1000)
        self.socket.close()
        print "data:",data
        results = {}
        print "positions"
        for line in data.split("\n"):
            line = line.strip()         
            if line=="":
                break
            else:
                name,x,y = line.split(" ")
                x = float(x)
                y = float(y)
                print name,"x",x,"y",y
                results[name] = [x,y]


        return results


if __name__=="__main__":
    client = PositionsClient(TCP_IP,TCP_PORT)


